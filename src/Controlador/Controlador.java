package Controlador;
import Modelo.Docentes;
import Vista.dlgPagosDocentes;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import javax.swing.JOptionPane;
import javax.swing.WindowConstants;


public class Controlador implements ActionListener{

    private Docentes docente;
    private dlgPagosDocentes vista;

    public Controlador(Docentes docentes, dlgPagosDocentes vista) {

        this.docente = docentes;
        this.vista = vista;

        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
    }
    
    
    private void iniciarVista() {
        vista.setTitle("Calculo de Pagos Docentes");
        vista.setSize(520, 530);
        vista.setLocationRelativeTo(null);
        vista.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        vista.setVisible(true);
        vista.dispose();
        System.exit(0);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista.btnCerrar) {
            int result = JOptionPane.showConfirmDialog(vista, "¿Seguro que deseas salir?", "Advertencia", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                System.exit(0);
            } else if (result == JOptionPane.NO_OPTION) {
                vista.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            }
        }
        
        
        if(e.getSource() == vista.btnNuevo){
           vista.txtDomicilioDocente.setEnabled(true);
           vista.txtHorasImpartidas.setEnabled(true);
           vista.txtNombreDocente.setEnabled(true);
           vista.txtNumeroDocente.setEnabled(true);
           vista.txtPagoHora.setEnabled(true);
       }
        
               
       if(e.getSource() == vista.btnLimpiar){
           vista.txtDomicilioDocente.setText("");
           vista.txtHorasImpartidas.setText("");
           vista.txtNombreDocente.setText("");
           vista.txtNumeroDocente.setText("");
           vista.txtPagoHora.setText("");
           vista.labDescuentoImpuesto.setText("");
           vista.labPagoBono.setText("");
           vista.labTotalAPagar.setText("");
           vista.labelPagoHoraImpartida.setText("");
           vista.spinNivelDocente.setValue("1 Licenciatura o Ingeniería");
           vista.spinHijos.setValue(0);
       }
       
       if(e.getSource() == vista.btnCancelar){
           vista.txtDomicilioDocente.setText("");
           vista.txtDomicilioDocente.setEnabled(false);
           vista.txtHorasImpartidas.setText("");
           vista.txtHorasImpartidas.setEnabled(false);
           vista.txtNombreDocente.setText("");
           vista.txtNombreDocente.setEnabled(false);
           vista.txtNumeroDocente.setText("");
           vista.txtPagoHora.setEnabled(false);
           vista.txtPagoHora.setText("");
           vista.labDescuentoImpuesto.setText("");
           vista.labPagoBono.setText("");
           vista.labTotalAPagar.setText("");
           vista.labelPagoHoraImpartida.setText("");
           vista.spinNivelDocente.setValue("1 Licenciatura o Ingeniería");
           vista.spinHijos.setValue(0);
       }
       
        if (e.getSource() == vista.btnMostrar) {
            vista.txtDomicilioDocente.setText(docente.getDomicilioDocente());
            vista.txtHorasImpartidas.setText(Integer.toString(docente.getHorasImpartidas()));
            vista.txtNombreDocente.setText(docente.getNombreDocente());
            vista.txtNumeroDocente.setText(Integer.toString(docente.getNumDocente()));
            vista.txtPagoHora.setText(Float.toString(docente.getPagoHoras()));
            vista.labPagoBono.setText(Float.toString(docente.calcularBono(0)));
            vista.labDescuentoImpuesto.setText(Float.toString(docente.calcularImpuesto()));
            vista.labPagoBono.setText(Float.toString(docente.calcularBono(0)));
            vista.spinNivelDocente.setValue(docente.getNivel());
            vista.spinHijos.setValue(docente.getHijos());
            float totalAPagar = (docente.calcularPago()+ docente.calcularBono((int) vista.spinHijos.getValue())) - docente.calcularImpuesto();
            vista.labTotalAPagar.setText(Float.toString(totalAPagar));
            vista.labelPagoHoraImpartida.setText(Float.toString(docente.calcularPago()));

       }
       
       if (e.getSource() == vista.btnGuardar){
           int hijos = (int) vista.spinHijos.getValue();
           docente.setHijos((int) vista.spinHijos.getValue());
           docente.setDomicilioDocente(vista.txtDomicilioDocente.getText());
           try{
           docente.setHorasImpartidas(Integer.parseInt(vista.txtHorasImpartidas.getText()));
           docente.setNivel(vista.spinNivelDocente.getValue().toString());
           docente.setNombreDocente(vista.txtNombreDocente.getText());
           docente.setNumDocente(Integer.parseInt(vista.txtNumeroDocente.getText()));
           docente.setPagoHoras(Float.parseFloat(vista.txtPagoHora.getText()));
           vista.labPagoBono.setText(Float.toString(docente.calcularBono(hijos)));
           vista.labDescuentoImpuesto.setText(Float.toString(docente.calcularImpuesto()));
           vista.labPagoBono.setText(Float.toString(docente.calcularBono(hijos)));
           vista.spinNivelDocente.setValue(docente.getNivel());
           }catch(NumberFormatException exc){
               JOptionPane.showMessageDialog(vista, "Error en la conversion de numero " + exc.getMessage());
           }
           float totalAPagar = (docente.calcularPago()+ docente.calcularBono(hijos)) - docente.calcularImpuesto();
           vista.labTotalAPagar.setText(Float.toString(totalAPagar));
           vista.labelPagoHoraImpartida.setText(Float.toString(docente.calcularPago()));
           if (docente.calcularPago() != 0) {
               vista.btnMostrar.setEnabled(true);
           }
       }
    }
    
    public static void main(String[] args) {
        Docentes docentes = new Docentes();
        dlgPagosDocentes vista = new dlgPagosDocentes(new JFrame(), true);
        Controlador control = new Controlador(docentes, vista);
        control.iniciarVista();
    }

}
