package Modelo;

public class Docentes {

    private int numDocente;
    private String nombreDocente;
    private String domicilioDocente;
    private String nivel;
    private Float pagoHoras;
    private int horasImpartidas;
    private int hijos;

    public Docentes() {

    }

    public Docentes(int numDocente, String nombreDocente, String domicilioDocente, String nivel, Float pagoHoras, int horasImpartidas) {
        this.numDocente = numDocente;
        this.nombreDocente = nombreDocente;
        this.domicilioDocente = domicilioDocente;
        this.nivel = nivel;
        this.pagoHoras = pagoHoras;
        this.horasImpartidas = horasImpartidas;
    }

    public Docentes(Docentes docentes) {

    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombreDocente() {
        return nombreDocente;
    }

    public int getHijos() {
        return hijos;
    }

    public void setHijos(int hijos) {
        this.hijos = hijos;
    }
    

    public void setNombreDocente(String nombreDocente) {
        this.nombreDocente = nombreDocente;
    }

    public String getDomicilioDocente() {
        return domicilioDocente;
    }

    public void setDomicilioDocente(String domicilioDocente) {
        this.domicilioDocente = domicilioDocente;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public Float getPagoHoras() {
        return pagoHoras;
    }

    public void setPagoHoras(Float pagoHoras) {
        this.pagoHoras = pagoHoras;
    }

    public int getHorasImpartidas() {
        return horasImpartidas;
    }

    public void setHorasImpartidas(int horasImpartidas) {
        this.horasImpartidas = horasImpartidas;
    }
    
    public float calcularPago() {
        float incremento;

        switch (this.nivel) {
            case "1 Licenciatura o Ingeniería":
                incremento = this.pagoHoras * 0.30f;
                break;
            case "2 Maestro en Ciencias":
                incremento = this.pagoHoras * 0.50f;
                break;
            case "3 Doctor":
                incremento = this.pagoHoras * 1.00f;
                break;
            default:
                incremento = 0.00f;
                break;
        }

        float pagoTotal = (this.pagoHoras + incremento) * this.horasImpartidas;
        return pagoTotal;
    }
    
    public float calcularImpuesto() {
        float pagoTotal = calcularPago();
        float impuesto = pagoTotal * 0.16f;
        return impuesto;
    }
    
    public float calcularBono(int hijos) {
        float porcentajeBono;
        if (hijos <= 2) {
            porcentajeBono = 0.05f;
        } else if (hijos <= 5) {
            porcentajeBono = 0.10f;
        } else {
            porcentajeBono = 0.20f;
        }
        float bono = calcularPago() * porcentajeBono;
        return bono;
    }
}
